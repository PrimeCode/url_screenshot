from selenium import webdriver

from PIL import Image
from resizeimage import resizeimage

DRIVER = 'chromedriver'
driver = webdriver.Chrome(DRIVER)
driver.maximize_window()
driver.get('https://www.google.com')
screenshot = driver.save_screenshot('my_screenshot.png')
driver.quit()

with open('my_screenshot.png', 'r+b') as f:
    with Image.open(f) as image:
        cover = resizeimage.resize_contain(image, [1024, 768])
        cover.save('my_screenshot.png', image.format)